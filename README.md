# it_addmodule-ceph
Ansible plays to install ceph cluster as a backend for Openstack.  
Can be run separetly or integrated to kolla-ansible playbook by simply
merging files to kolla-ansible project.

Project is based on cephadm-ansible project:  
https://github.com/jcmdln/cephadm-ansible

Running separately
===================
Environment
-------------------
1. Deployment node  
Physical server or VM. 
Host OS:
- Ubuntu Bionic
- CentOS 8 (not tested)
- Debian Buster (not tested)  
Installed Python3, Ansible >=2.9,<3  

2. Cluster nodes  
Physical servers with disks for Ceph OSD. Can be VMs for testing
environment.  
Host OS:
- Ubuntu Bionic
- CentOS 8 (not tested)
- Debian Buster (not tested)  

Passwordless SSH login from Deployment server to Cluster nodes enabled.  
Names resolvable by DNS or are written to /etc/hosts on each server.

Setup
-------------------
```
git clone https://gitlab.com/kemopq/it_addmodule-ceph.git
cd it_addmodule-ceph
```

Configure
-------------------
1. Create your inventory  
Template is on ansible/inventory/inventory  
Set at least mon nodes.   

2. Define your configuration  
Template is on etc/kolla/globals.yml  
Set at least path to kolla-ansible configuration of your 
Openstack cluster to variable: node_custom_config  

3. Configure OSDs  
OSDs are configured in globals.yml in an OSD Service Specification 
notation:
https://docs.ceph.com/docs/master/cephadm/drivegroups/#drivegroups

If disk partitions or LVM logical volumes are used for OSD disks, 
paths to those partitions can be listed in OSD Service Specification. 
It works for data_devices, but not for db_devices and wal_devices.
(see: https://tracker.ceph.com/issues/46558).
```
service_type: osd
service_id: osd_spec_default
placement:
  host_pattern: 'vmcloud-node1'
data_devices:
  paths:
  - /dev/vdb1
  - /dev/vdb2
```

4. Define admin password of Ceph dashboard  
Template is on etc/kolla/passwords.yml

Deploying
-------------------
1. Run the playbook
    ```
    cd tools
    ./kolla-ansible-cephadm --inventory ${PATH_TO_INVENTORY}/inventory --configdir ${PATH_TO_CONFIGURATION} --password ${PATH_TO_PASSWORD}/passwords.yml bootstrap-servers
    ./kolla-ansible-cephadm --inventory ${PATH_TO_INVENTORY}/inventory --configdir ${PATH_TO_CONFIGURATION} --password ${PATH_TO_PASSWORD}/passwords.yml deploy
    ```
Checking deployment
-------------------
1. On cluster server
    ```
    ceph osd pool ls
    ```
    Listed pools: vms,volumes,backups,images  

2. On deployment server  
Check files on "node_custom_config":  
nova/ceph.conf  
nova/ceph.client.cinder.keyring  
cinder/ceph.conf  
cinder/cinder-backup/ceph.client.cinder.keyring  
cinder/cinder-backup/ceph.client.cinder-backup.keyring  
cinder/cinder-volume/ceph.client.cinder.keyring  
glance/ceph.conf  
glance/ceph.client.glance.keyring  


Running as part of kolla-ansible playbook
===================
Setup
-------------------
```
git clone https://gitlab.com/kemopq/it_addmodule-ceph.git
cd it_addmodule-ceph
```

Merging with kolla-ansible playbook
-------------------
Cephadm is additional role in kolla-ansible playbook.  
Common files should be appended to kolla-ansible files 
(site-pre.yml to the beginning of site.yml before inclusion of first role).  
For configuration file use globals-for-kolla-ansible.yml  
For inventory use inventory-for-kolla-ansible.  
cephadm role files should be copied to ansible/roles dir.  


Configure
-------------------
1. Create your inventory

kolla-ansible inventory is used

2. Define your configuration

kolla-ansible configuration is used

3. Configure OSDs  
OSDs are configured in globals.yml in an OSD Service Specification 
notation:
https://docs.ceph.com/docs/master/cephadm/drivegroups/#drivegroups

If disk partitions or LVM logical volumes are used for OSD disks, 
paths to those partitions can be listed in OSD Service Specification. 
It works for data_devices, but not for db_devices and wal_devices.
(see: https://tracker.ceph.com/issues/46558).
```
service_type: osd
service_id: osd_spec_default
placement:
  host_pattern: 'vmcloud-node1'
data_devices:
  paths:
  - /dev/vdb1
  - /dev/vdb2
```
4. Define admin password of Ceph dashboard

kolla-ansible passwords file is used

Deploying
-------------------
1. Ceph cluster is deployed within kolla-ansible bootstrap-servers and deploy actions.


Current state
===================
Tested:  
- TESTING ENVIRONMENT:  
  Deployment server: Ubuntu bionic VM on KVM, Python 3.6.9, Ansible 2.9.11  
  Cluster servers: 3x Ubuntu bionic VMs on KVM, each VM has additional disk for Ceph OSD  
- ACTION: deploying Ceph cluster  
- OPENSTACK MODULE INTEGRATION: Openstack Ussuri; nova, cinder, glance modules
- KNOWN PROBLEMS: cinder backup doesn't work because of:  
  https://review.opendev.org/#/c/730376/  
  Can be solved by changing file in cinder_backup container:  
  /var/lib/kolla/venv/lib/python3.6/site-packages/os_brick/initiator/connectors/rbd.py  
  function: create_ceph_conf  
  add "[global]\n" to command:  
  conf_file.writelines([ "[global]\n", mon_hosts, "\n", keyring, "\n"])  

